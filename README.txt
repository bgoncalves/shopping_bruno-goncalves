
HOW TO RUN THE APPLICATION

OPTION 1 - with local server:

DEPENDENCIES:
 - nodeJs
 - npm

STEPS:
 - Open a console and navigate to the project folder
 - type and run: "npm install"
 - type and run: "gulp build-prod"

OPTION 2 - without local server

STEPS:
Open the dist folder and run index.html in the browser(bag won't work on EDGE)

