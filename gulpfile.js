var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var useref = require('gulp-useref');
var minify = require('gulp-minify');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cleanCSS = require('gulp-clean-css');
var sass = require('gulp-sass');

/* DEV */
gulp.task('browserSync', function () {
    browserSync.init({
        server: {
            baseDir: 'app'
        }
    });
});

gulp.task('watch', ['browserSync'], function () {
    gulp.watch('app/index.html', browserSync.reload);
    gulp.watch('app/js/*.js', browserSync.reload);
    gulp.watch('app/css/*.css', browserSync.reload);
    gulp.watch('app/sass/*.scss',['styles']);
});

gulp.task('styles', function() {
    gulp.src('app/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('app/css/'));
});

gulp.task('build-dev', ['watch'], function (){
  console.log('Building files for dev');
});

/* END DEV */

/* PROD */

gulp.task('browserSync-prod', function () {
    browserSync.init({
        server: {
            baseDir: 'dist'
        }
    });
});

gulp.task('optimize', function(){
  return gulp.src('app/index.html')
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulpIf('*.css', cleanCSS()))
    .pipe(gulp.dest('dist'));
});

gulp.task('copyAssets', function(){
    gulp.src(['app/images/*'])
    .pipe(gulp.dest('dist/images'))
    gulp.src(['app/css/fonts/*'])
    .pipe(gulp.dest('dist/css/fonts/'));
});

gulp.task('build-prod', ['optimize', 'copyAssets', 'browserSync-prod'], function (){
  console.log('Building files for prod');
});
/* END PROD */
