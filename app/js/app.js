
var products = [
    {
        id: 1,
        itemImgSrc: 'images/11730556_8194858_255.jpg',
        itemBrand: 'Armani',
        itemDesc: 'Great pants by Armany',
        itemPrice: '125'
    },
    {
        id: 2,
        itemImgSrc: 'images/11734827_8179707_255.jpg',
        itemBrand: 'Prada',
        itemDesc: 'Beautifull Prada shoes',
        itemPrice: '250'
    },
    {
        id: 3,
        itemImgSrc: 'images/11735583_8161812_255.jpg',
        itemBrand: 'DKNY',
        itemDesc: 'Beautifull dress by DKNY',
        itemPrice: '358'
    },
    {
        id: 4,
        itemImgSrc: 'images/11736243_8231608_255.jpg',
        itemBrand: 'GUESS',
        itemDesc: 'Great jeand from GUESS',
        itemPrice: '453'
    },
    {
        id: 5,
        itemImgSrc: 'images/11745743_8214817_255.jpg',
        itemBrand: 'Moschino',
        itemDesc: 'Amazing shirt by moschino',
        itemPrice: '5'
    }
];

function getProductById(id) {
    var returnObj = {};
    for (var key in products) {
        if (id === products[key].id) {
            returnObj = products[key];
            break;
        }
    }
    return returnObj;
}

function ItemsTmpl(tmplId) {
    this.tmplId = tmplId;
}

ItemsTmpl.prototype.getItemsTmpl = function () {
    var tmplObj = document.getElementById(this.tmplId);
    if (tmplObj) {
        return tmplObj.innerHTML;
    }
};

ItemsTmpl.prototype.buildItemsTmpl = function (objArr, type) {
    var tmplHTML = this.getItemsTmpl();
    var buttonText, buttonAction, itemHTML = '';

    if (type === 1) {
        buttonText = 'Add to bag';
        buttonAction = 'addBagItem';
    } else {
        buttonText = 'Remove from bag';
        buttonAction = 'rmBagItem';
    }

    if (tmplHTML) {
        for (var key in objArr) {
            itemHTML += tmplHTML
                    .replace(/{{itemImgSrc}}/g, objArr[key]["itemImgSrc"] ? objArr[key]["itemImgSrc"] : '')
                    .replace(/{{itemBrand}}/g, objArr[key]["itemBrand"] ? objArr[key]["itemBrand"] : '-')
                    .replace(/{{itemDesc}}/g, objArr[key]["itemDesc"] ? objArr[key]["itemDesc"] : '-')
                    .replace(/{{itemPrice}}/g, objArr[key]["itemPrice"] ? objArr[key]["itemPrice"] : '-')
                    .replace(/{{itemId}}/g, objArr[key]["id"] ? objArr[key]["id"] : 0)
                    .replace(/{{buttonAction}}/g, buttonAction)
                    .replace(/{{buttonText}}/g, buttonText);
        }
    }
    return itemHTML;
};

function Bag() {
    this.itemsCount = 0;
    this.itemsValue = 0;
}

Bag.prototype.getItems = function () {
    var bagStr = localStorage.getItem("shoppBag");
    if (!bagStr) {
        return this.init();
    }
    return JSON.parse(bagStr);
};

Bag.prototype.addItem = function (id) {
    if (this.bagHasItem(id) !== -1) {
        return;
    }
    var item = getProductById(id);
    if (Object.keys(item).length !== 0) {
        var bagItems = this.getItems();
        bagItems.push(item);
        localStorage.setItem('shoppBag', JSON.stringify(bagItems));
    }
    updateUI();
};
Bag.prototype.removeItem = function (index) {
    var bagItems = this.getItems();
    bagItems.splice(index, 1);
    localStorage.setItem('shoppBag', JSON.stringify(bagItems));
    updateUI();
};

Bag.prototype.bagHasItem = function (id) {
    var itemExists = -1;
    var bagItems = this.getItems();
    for (var key in bagItems) {
        if (id === bagItems[key].id) {
            itemExists = key;
            break;
        }
    }
    return itemExists;
};

Bag.prototype.getItemsCount = function (id) {
    var bagItems = this.getItems();
    return bagItems.length;
};

Bag.prototype.getItemsValue = function (id) {
    var itemsValue = 0;
    var bagItems = this.getItems();
    for (var key in bagItems) {
        itemsValue = itemsValue + +bagItems[key].itemPrice;
    }
    return itemsValue;
};

Bag.prototype.init = function () {
    var emptyBag = [];
    localStorage.setItem('shoppBag', JSON.stringify(emptyBag));
    return emptyBag;
};

var bag = new Bag();
function rmBagItem(id, action) {
    var itemIndex = bag.bagHasItem(id);
    if (itemIndex !== -1) {
        bag.removeItem(itemIndex);
    }
}

function addBagItem(id) {
    var itemIndex = bag.bagHasItem(id);
    if (itemIndex === -1) {
        bag.addItem(id);
    } else {
        alert('Item already in bag');
    }
}

var tmplO = new ItemsTmpl("item-template");
function updateUI() {
    
    var tmpl = tmplO;
    document.getElementById("items-list").innerHTML = tmpl.buildItemsTmpl(products, 1);

    var bagItems = bag.getItems();
    if(bagItems.length === 0){
        var noItems = '<div class="no-items">You don\'t have any items in your bag.</div>';
        document.getElementById("cart-list").innerHTML = noItems;
    } else {
        document.getElementById("cart-list").innerHTML = tmpl.buildItemsTmpl(bagItems, 2);
    }  

    document.getElementById("cart-quant").innerHTML = bag.getItemsCount();
    document.getElementById("cart-value").innerHTML = bag.getItemsValue();
}

window.onload = function () {
    updateUI();
};
